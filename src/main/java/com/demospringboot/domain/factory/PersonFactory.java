package com.demospringboot.domain.factory;

public interface PersonFactory {
    void createPerson();
}
